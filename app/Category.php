<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name', 'photo_id','body'];

    public function photo()
    {
        return $this->belongsTo('App\Photo');
    }

    public function post(){
        return $this->hasMany('App\Post');
    }
    public function question(){
        return $this->hasMany('App\Question');
    }
}
