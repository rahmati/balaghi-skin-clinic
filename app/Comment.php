<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'body',
        'parent_id',
        'photo',
        'user_id',
        'email',
        'is_active',
        'email',
        'commentable_id',
        'commentable_type'
    ];

    public function commentable()
    {
        return $this->morphTo();
    }

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }


    public function parent()
    {
        return $this->belongsTo('App\Comment', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Comment', 'parent_id','id');
    }

    public  function  post()
    {
        return $this->belongsTo('App\Post','commentable_id');
    }

    public  function  question()
    {
        return $this->belongsTo('App\Question','commentable_id');
    }
}
