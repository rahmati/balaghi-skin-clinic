<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enum extends Model
{
    protected $fillable=[
        'category',
        'name',
        'value',
        'description',
        ];

}
