<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\categoryRequest;
use App\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AdminCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'لیست خدمات کلینیک';
        $subTitle = 'در اینجا می توانید کلیه خدمات کلینیک را مشاهده کنید ';
        $limit = 10;
        $query = Category::query();

        if (!empty($request->text)) {
            $text = $request->text;
            $query = $query->where(function ($query) use ($text) {
                $query->orwhere('name', 'LIKE', '%' . $text . '%');
            });
        }

        if (!empty($request->limit)) {

            $limit = $request->limit;
        }

        if ($request->ajax()) {

            $categories = $query->paginate($limit)->appends(array('text' => !empty($text) ? $text : ''));

            return response()->json(array(
                'body' => view('Elements/category', compact('categories', 'text'))->render()

            ), JSON_UNESCAPED_UNICODE);

        }

        $categories = $query->paginate($limit)->appends(array('text' => !empty($text) ? $text : ''));

        return view('admin.categories.index', compact('title', 'subTitle', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'ایجاد خدمات کلینیک';
        $subTitle = 'در اینجا می توانید خدمات کلینیک راایجاد کنید';
        return view('admin.categories.create', compact('title', 'subTitle'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(categoryRequest $request)
    {

        $input['name'] = $request->name;
        $input['body'] = $request->body;

        if ($file = $request->file('photo_id')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $photo = Photo::create(['image' => $name]);
            $input['photo_id'] = $photo->id;
        }

        Category::create($input);
        Session::flash('store', 'با موفقیت ذخیره شد');
        return redirect('admin/categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = 'ویرایش';
        $subTitle = 'در اینجا می توانید این خدمت کلینیک را ویرایش کنید';
        $category=Category::findOrFail($id);
        return view('admin.categories.edit', compact('title', 'subTitle','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(categoryRequest $request, $id)
    {

        $input['name'] = $request->name;
        $category=Category::findOrFail($id);

        if ($file = $request->file('photo_id')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $photo = Photo::create(['image' => $name]);
            $input['photo_id'] = $photo->id;
        }

        $category->update($input);
        Session::flash('update', 'با موفقیت به روزرسانی شد');
        return redirect('admin/categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if ($request->ajax()) {

            $category = Category::findOrFail($request->id);
            if($category->delete())
            {
                return response()->json(array(
                    'status' =>'1',
                    'message'=>'رکورد شما با موفقیت حذف شد'

                ), JSON_UNESCAPED_UNICODE);
            }else{
                return response()->json(array(
                    'status' =>'0',
                    'message'=>'مشکلی در سرور بهوجود آمده است لطفا دوباره تلاش کنید'

                ), JSON_UNESCAPED_UNICODE);
            }
        }

    }

}
