<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AdminCommentsController extends Controller
{

    public function __construct()
    {

//        $this->middleware('admin')->except('create', 'store','memberComments');
//        $this->middleware('member')->except('index', 'edit', 'update', 'destroy');
//        $this->middleware('member')->only('create', 'store','memberComments');
//        $this->middleware('admin')->only('index', 'edit', 'update', 'destroy');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'لیست کامنت های سایت';
        $subTitle = 'در اینجا می توانید کلیه کامنت های سایت را مشاهده کنید ';
        $limit = 10;
        $query = Comment::query();

        if (!empty($request->text)) {
            $text = $request->text;
            $query = $query->where(function ($query) use ($text) {
                $query->orwhere('body', 'LIKE', '%' . $text . '%');
            });
        }

        if (!empty($request->limit)) {

            $limit = $request->limit;
        }

        if ($request->ajax()) {

            $comments = $query->paginate($limit)->appends(array('text' => !empty($text) ? $text : ''));

            return response()->json(array(
                'body' => view('Elements/comments', compact('comments', 'text'))->render()

            ), JSON_UNESCAPED_UNICODE);

        }

        $comments = $query->paginate($limit)->appends(array('text' => !empty($text) ? $text : ''));

        return view('admin.comments.index', compact('title', 'subTitle', 'comments'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'لیست کامنت های پست';
        $subTitle = 'در اینجا می توانید کلیه کامنت های پست شماره ' . $id . ' را مشاهده کنید ';
        $limit = 10;
        $query = Comment::query();
        $query = $query->where('commentable_id',$id)
        ->where('commentable_type','App\Post');

        if (!empty($request->text)) {
            $text = $request->text;
            $query = $query->where(function ($query) use ($text) {
                $query->orwhere('body', 'LIKE', '%' . $text . '%');
            });
        }

        if (!empty($request->limit)) {

            $limit = $request->limit;
        }

        if ($request->ajax()) {

            $comments = $query->paginate($limit)->appends(array('text' => !empty($text) ? $text : ''));

            return response()->json(array(
                'body' => view('Elements/comments', compact('comments', 'text'))->render()

            ), JSON_UNESCAPED_UNICODE);

        }

        $comments = $query->paginate($limit)->appends(array('text' => !empty($text) ? $text : ''));

        return view('admin.comments.show', compact('comments', 'title', 'subTitle'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->ajax()){

            $comment = Comment::findOrFail($request->id);
            $request['is_active']=$request->active==1?0:1;
            $comment->update($request->all());
            return response()->json(array(
                'status' => 1,
                'message'=>'با موفقیت تغییر کرد'

            ), JSON_UNESCAPED_UNICODE);
        }
        $comment = Comment::findOrFail($id);
        $comment->update($request->all());

        return redirect('admin/comments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Comment::findOrFail($id)->delete();
        return redirect()->back();
    }

    public function createComment(Request $request)
    {

        $user = Auth::user();

        $data = [
            'commentable_id' => $request->post_id,
            'user_id' => $user->id,
            'email' => $user->email,
            'photo' => $user->photo ? $user->photo->image : '',
            'body' => $request->body,
            'parent_id' => $request->parent_id,
            'commentable_type'=>'App\Post'
        ];


        Comment::create($data);
        $request->session()->flash('store', 'پیام شما با موفقیت ثبت شد');
        return redirect()->back();


    }

    public function memberComments(){

        $user=Auth::user();
        $comments=Comment::all()->where('user_id',$user->id);
        return view('membership.comments.comments',compact('comments'));
    }

}
