<?php

namespace App\Http\Controllers;

use App\Enum;
use App\Http\Requests\EnumRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;

class AdminEnumsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'لیست متغیرهای مختلف مدیریت';
        $subTitle = 'در اینجا می توانید کلیه نقش ها و متغیرهای مختلف پنل را مشاهده کنید ';
        $limit = 10;
        $query = Enum::query();

        if (!empty($request->text)) {
            $text = $request->text;
            $query = $query->where(function ($query) use ($text) {
                $query->orwhere('category', 'LIKE', '%' . $text . '%')
                    ->orwhere('name', 'LIKE', '%' . $text . '%')
                    ->orwhere('value', 'LIKE', '%' . $text . '%')
                    ->orwhere('description', 'LIKE', '%' . $text . '%');
            });
        }

        if (!empty($request->limit)) {

            $limit = $request->limit;
        }

        if ($request->ajax()) {

            $enums = $query->paginate($limit)->appends(array('text' => !empty($text) ? $text : ''));

            return response()->json(array(
                'body' => view('Elements/enums', compact('enums', 'text'))->render()

            ), JSON_UNESCAPED_UNICODE);

        }

        $enums = $query->paginate($limit)->appends(array('text' => !empty($text) ? $text : ''));

        return view('admin.enums.index', compact('title', 'subTitle', 'enums'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'لیست پارامترهای مختلف سایت';
        $subTitle = 'در اینجا می توانید کلیه پارامترهای سایت را مشاهده کنید ';
        return view('admin.enums.create', compact('title', 'subTitle'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(EnumRequest $request)
    {
        $data = $request->all();
        Enum::create($data);
        Session::flash('store', 'با موفقیت ذخیره شد');
        return redirect('admin/enums');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = 'ویرایش پارامترهای مختلف سایت';
        $subTitle = 'در اینجا می توانید این پارامتر را ویرایش کنید ';
        $enum = Enum::findOrFail($id);
        return view('admin.enums.edit', compact('title', 'subTitle', 'enum'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EnumRequest $request, $id)
    {
        $enum = Enum::findOrFail($id);
        $enum->update($request->all());
        Session::flash('update', 'با موفقیت به روزرسانی شد');
        return redirect('admin/enums');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if ($request->ajax()) {

            $enum =Enum::findOrFail($request->id);
            if($enum->delete())
            {
                return response()->json(array(
                    'status' =>'1',
                    'message'=>'رکورد شما با موفقیت حذف شد'

                ), JSON_UNESCAPED_UNICODE);
            }else{
                return response()->json(array(
                    'status' =>'0',
                    'message'=>'مشکلی در سرور بهوجود آمده است لطفا دوباره تلاش کنید'

                ), JSON_UNESCAPED_UNICODE);
            }
        }
    }
}
