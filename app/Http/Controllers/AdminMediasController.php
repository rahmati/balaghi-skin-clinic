<?php

namespace App\Http\Controllers;

use App\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminMediasController extends Controller
{

    public function index(Request $request)
    {

        $title = 'لیست تصاویر سایت';
        $subTitle = 'در اینجا می توانید کلیه تصاویر پنل را مشاهده کنید ';
        $limit=10;
        $query =Photo::query();

        if (!empty($request->text)) {
            $text = $request->text;
            $query = $query->where(function ($query) use ($text) {
                $query->orwhere('image', 'LIKE', '%' . $text . '%');
            });
        }

        if (!empty($request->limit)) {

            $limit = $request->limit;
        }


        if ($request->ajax()) {

            $photos= $query->paginate($limit)->appends(array('text' => !empty($text) ? $text : ''));

            return response()->json(array(
                'body' => view('Elements/media', compact('photos', 'text'))->render()

            ), JSON_UNESCAPED_UNICODE);

        }


        $photos=$query->paginate($limit)->appends(array('text' => !empty($text) ? $text : ''));

        return view('admin.media.index', compact('photos', 'title', 'subTitle','text'));

    }

    public function create()
    {
        $title = 'لیست تصاویر سایت';
        $subTitle = 'در اینجا می توانید کلیه تصاویر پنل را مشاهده کنید ';
        return view('admin.media.create', compact('photos', 'title', 'subTitle'));
    }

    public function store(Request $request)
    {
        $file = $request->file('image');
        var_dump($file);
        $name = time() . $file->getClientOriginalName();
        $file->move('images', $name);
        Session::flash('store', 'با موفقیت ذخیره شد');
        Photo::create(['image' => $name]);

    }

    public function destroy(Request $request)
    {
        if ($request->ajax()) {

            $photo = Photo::findOrFail($request->id);
            unlink(public_path() . $photo->image);
            if($photo->delete())
            {
                return response()->json(array(
                    'status' =>'1',
                    'message'=>'رکورد شما با موفقیت حذف شد'

                ), JSON_UNESCAPED_UNICODE);
            }else{
                return response()->json(array(
                    'status' =>'0',
                    'message'=>'مشکلی در سرور به وجود آمده است لطفا دوباره تلاش کنید'

                ), JSON_UNESCAPED_UNICODE);
            }
        }
    }

}
