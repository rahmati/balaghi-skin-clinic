<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Http\Requests\PostRequest;
use App\Photo;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Symfony\Component\VarDumper\Dumper\DataDumperInterface;


class AdminPostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'لیست مطالب سایت';
        $subTitle = 'در اینجا می توانید کلیه مطالب سایت را مشاهده کنید ';
        $limit = 10;
        $query = Post::query();

        if (!empty($request->text)) {
            $text = $request->text;
            $query = $query->where(function ($query) use ($text) {
                $query->orwhere('title', 'LIKE', '%' . $text . '%')
                ->orwhere('body', 'LIKE', '%' . $text . '%');
            });
        }

        if (!empty($request->limit)) {

            $limit = $request->limit;
        }

        if ($request->ajax()) {

            $posts = $query->paginate($limit)->appends(array('text' => !empty($text) ? $text : ''));

            return response()->json(array(
                'body' => view('Elements/posts', compact('posts', 'text'))->render()

            ), JSON_UNESCAPED_UNICODE);

        }

        $posts = $query->paginate($limit)->appends(array('text' => !empty($text) ? $text : ''));
        return view('admin.posts.index', compact('posts', 'title', 'subTitle'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'ایجاد مطلب جدید';
        $subTitle = 'در اینجا می توانید مطلب جدیدی برای سایت را ایجاد کنید ';
        $users = DB::table('users')->where('enum_user_group_id', 1)->pluck('last_name','id')->all();
        $categories = Category::pluck('name','id')->all();
        return view('admin.posts.create', compact('title', 'subTitle', 'users', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $input = $request->all();
        if ($file = $request->file('photo_id')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $photo = Photo::create(['image' => $name]);
            $input['photo_id'] = $photo->id;
        }


        Post::create($input);
        Session::flash('store','با موفقیت ذخیره شد');
        return redirect('admin/posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = 'ایجاد مطلب جدید';
        $subTitle = 'در اینجا می توانید مطلب جدیدی برای سایت را ایجاد کنید ';
        $users = DB::table('users')->where('enum_user_group_id', 1)->pluck('last_name','id')->all();
        $categories = Category::pluck('name','id')->all();
        $post=Post::findOrFail($id);
        return view('admin.posts.edit', compact('title', 'subTitle','post','users', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $id)
    {
        $input = $request->all();
        if ($file = $request->file('photo_id')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $photo = Photo::create(['image' => $name]);
            $input['photo_id'] = $photo->id;
        }
        $post=Post::findOrFail($id);
        $post->update($input);
        Session::flash('update','با موفقیت به روزرسانی شد');
        return redirect('admin/posts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(Request $request)
    {

        if ($request->ajax()) {

            $post=Post::findOrFail($request->id);
            if($post->delete())
            {
                return response()->json(array(
                    'status' =>'1',
                    'message'=>'رکورد شما با موفقیت حذف شد'

                ), JSON_UNESCAPED_UNICODE);
            }else{
                return response()->json(array(
                    'status' =>'0',
                    'message'=>'مشکلی در سرور به وجود آمده است لطفا دوباره تلاش کنید'

                ), JSON_UNESCAPED_UNICODE);
            }
        }

    }

    public function post($slug)
    {

        $post=Post::findBySlugOrFail($slug);
        $comments = $post->comment()->whereIsActive(1)->get();
        $categories=Category::all();
        $title=$post->title;

        return view('post',compact('post','comments','categories','title'));

    }
}
