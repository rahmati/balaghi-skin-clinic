<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\QuestionRequest;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class AdminQuestionsController extends Controller
{

    public function __construct()
    {

        $this->middleware('admin')->except('create', 'store','questions');
        $this->middleware('member')->except('index', 'edit', 'update', 'destroy');
        $this->middleware('member')->only('create', 'store','questions');
        $this->middleware('admin')->only('index', 'edit', 'update', 'destroy');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'لیست سوالات کاربران';
        $subTitle = 'در اینجا می توانید کلیه سوالات کاربران را مشاهده کنید ';
        $limit = 10;
        $query = Question::query();

        if (!empty($request->text)) {
            $text = $request->text;
            $query = $query->where(function ($query) use ($text) {
                $query->orwhere('question', 'LIKE', '%' . $text . '%')
                    ->orwhere('answer', 'LIKE', '%' . $text . '%');
            });
        }

        if (!empty($request->limit)) {

            $limit = $request->limit;
        }

        if ($request->ajax()) {

            $questions = $query->paginate($limit)->appends(array('text' => !empty($text) ? $text : ''));

            return response()->json(array(
                'body' => view('Elements/questions', compact('questions', 'text'))->render()

            ), JSON_UNESCAPED_UNICODE);

        }

        $questions = $query->paginate($limit)->appends(array('text' => !empty($text) ? $text : ''));
        return view('admin.questions.index', compact('questions', 'title', 'subTitle'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('name','id')->all();
        return view('membership.questions.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(QuestionRequest $request)
    {
        $user = Auth::user();
        $data = $request->all();
        $data['patient_id'] = $user->id;
        $data['doctor_id'] = 0;
        $data['answer'] = '';

        Question::create($data);
        Session::flash('store', 'سوال شما با موفقیت ذخیره شد');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = 'لیست سوالات کاربران';
        $subTitle = 'در اینجا می توانید کلیه سوالات کاربران را مشاهده کنید ';
        $question = Question::findOrFail($id);
        return view('admin.questions.edit', compact('title', 'subTitle', 'question'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(QuestionRequest $request, $id)
    {
        $user = Auth::user();
        $data = $request->all();
        $data['doctor_id'] = $user->id;
        $question = Question::findOrFail($id);
        $question->update($data);
        return redirect('admin/questions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(Request $request)
    {

        if ($request->ajax())
        {

            $question=Question::findOrFail($request->id);
            if($question->delete())
            {
                return response()->json(array(
                    'status' =>'1',
                    'message'=>'رکورد شما با موفقیت حذف شد'

                ), JSON_UNESCAPED_UNICODE);
            }else{
                return response()->json(array(
                    'status' =>'0',
                    'message'=>'مشکلی در سرور به وجود آمده است لطفا دوباره تلاش کنید'

                ), JSON_UNESCAPED_UNICODE);
            }
        }

    }

    public function questions(){

        $user=Auth::user();
        $questions=Question::all()->where('patient_id',$user->id);
        return view('membership.questions.questions',compact('questions'));
    }
}
