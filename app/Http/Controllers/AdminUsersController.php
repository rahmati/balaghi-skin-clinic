<?php

namespace App\Http\Controllers;

use App\Enum;
use App\Http\Requests\adminCreateRequest;
use App\Photo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;


/**
 * @property \Validator|\ValidatorComponent Validator
 */
class AdminUsersController extends Controller
{

    public function __construct()
    {

        $this->middleware('admin')->except('memberUpdate', 'changePassword');
        $this->middleware('member')->only('memberUpdate', 'changePassword');

    }

    public function index(Request $request)
    {
        $title = 'لیست کاربران مدیریت';
        $subTitle = 'در اینجا می توانید کلیه کاربران پنل را مشاهده کنید ';
        $limit=10;
        $query = User::with(['enum', 'photo'])->where('enum_user_group_id', '1');

        if (!empty($request->text)) {
            $text = $request->text;
            $query = $query->where(function ($query) use ($text) {
                $query->orwhere('first_name', 'LIKE', '%' . $text . '%')
                    ->orWhere('last_name', 'LIKE', '%' . $text . '%')
                    ->orWhere('email', 'LIKE', '%' . $text . '%');
            });
        }

        if (!empty($request->limit)) {

            $limit = $request->limit;
        }


        if ($request->ajax()) {

            $users = $query->paginate($limit)->appends(array('text' => !empty($text) ? $text : ''));

            return response()->json(array(
                'body' => view('Elements/users', compact('users', 'text'))->render()

            ), JSON_UNESCAPED_UNICODE);

        }


        $users = $query->paginate($limit)->appends(array('text' => !empty($text) ? $text : ''));

        return View('admin.users.index', compact('title', 'subTitle', 'users', 'text'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'ایجاد کاربر جدید';
        $subTitle = 'در اینجا کابر پنل جدیدی ایجاد کنید';
        $roles = DB::table('enums')->where('category', 'UserGroup')->pluck('value', 'id')->all();
        return view('admin.users.create', compact('title', 'subTitle', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(adminCreateRequest $request)
    {

        if (trim($request->password) == '') {
            $input = $request->except('password');
        } else {
            $input = $request->all();

            $input['password'] = bcrypt($request->password);
        }

        if ($file = $request->file('photo_id')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $photo = Photo::create(['image' => $name]);
            $input['photo_id'] = $photo->id;
        }

        $input['password'] = bcrypt($request->password);
        User::create($input);
        Session::flash('store_user', 'با موفقیت ذخیره شد');
        return redirect('admin/users');
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = 'ویرایش';
        $subTitle = 'در اینجا می توانید کاربران را ویرایش کنید';
        $roles = DB::table('enums')->where('category', 'UserGroup')->pluck('value', 'id')->all();
        $user = User::findOrFail($id);
        return view('admin.users.edit', compact('user', 'title', 'subTitle', 'roles'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(adminCreateRequest $request, $id)
    {

        $user = User::findOrFail($id);

        if (trim($request->password) == '') {
            $input = $request->except('password');
        } else {
            $input = $request->all();
            $input['password'] = bcrypt($request->password);
        }

        if ($file = $request->file('photo_id')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $photo = Photo::create(['image' => $name]);
            $input['photo_id'] = $photo->id;
        }

        $user->update($input);
        Session::flash('update_user', 'با موفقیت به روزرسانی شد');
        return redirect('admin/users');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        if ($request->ajax()) {

            $user = User::findOrFail($request->id);
           if($user->delete())
           {
               return response()->json(array(
                   'status' =>'1',
                   'message'=>'رکورد شما با موفقیت حذف شد'

               ), JSON_UNESCAPED_UNICODE);
           }else{
               return response()->json(array(
                   'status' =>'0',
                   'message'=>'مشکلی در سرور بهوجود آمده است لطفا دوباره تلاش کنید'

               ), JSON_UNESCAPED_UNICODE);
           }
        }

    }

    public function memberUpdate(Request $request)
    {

        $user = Auth::user();
        if ($request->isMethod('post')) {

            if (trim($request->password) == '') {
                $input = $request->except('password');
            } else {
                $input = $request->all();
                $input['password'] = bcrypt($request->password);
            }

            if ($file = $request->file('photo_id')) {
                $name = time() . $file->getClientOriginalName();
                $file->move('images', $name);
                $photo = Photo::create(['image' => $name]);
                $input['photo_id'] = $photo->id;
            }

            $user->update($input);
            Session::flash('update_user', 'با موفقیت به روزرسانی شد');
            return redirect()->back();

        }
        return view('membership.profile.create', compact('user'));
    }

    public function changePassword(Request $request)
    {
        if ($request->isMethod('post')) {

            $user = Auth::user();
            $oldPass = $user->password;

            if (Hash::check($request->password, $oldPass)) {
                $validatedData = $request->validate([
                    'new_password' => 'required|different:password|min:6',
                ]);
                if (trim($request->password) == '') {
                    $input = $request->except('password');
                } else {
                    $input = $request->all();
                    $input['password'] = bcrypt($request->new_password);
                }
                $user->update($input);


            } else {
                Session::flash('wrong-password', 'لطفا رمزعبور فعلی خود را درست وارد کنید');
            }

        }
        Session::flash('password', 'رمز عبور با موفقیت تغییر کرد');
        return view('membership.profile.changePassword');
    }

}
