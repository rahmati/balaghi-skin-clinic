<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $redirectAfterLogout = '/home';
    protected $redirectTo = '/home';

    public function redirectTo(){
        // User role
        $user =Auth::user();
        $role=$user->enum->name;

        // Check user role
        switch ($role) {
            case 'Admin':
                return '/admin/users';
                break;
            case 'Member':
                return '/membership/profile/memberProfile';
                break;
            default:
                return 'public/';
                break;
        }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

}
