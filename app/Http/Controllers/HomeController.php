<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Question;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $categories=Category::all();
        $title='کلینیک پوست و زیبایی دکتر بلاغی';

        return view('home',compact('categories','title'));
    }

    public function services()
    {
        $categories=Category::all();
        $title='خدمات کلینیک ';
        return view('services',compact('categories','title'));
    }

    public function question()
    {
        $categories =Category::with('question')->get();
        $title='سوالات متداول';
        return view('questions',compact('categories','title'));
    }

    public function questionAnswer($id=null)
    {
        $question=Question::findOrFail($id);
        $title='سوال وجواب';
        return view('questionAnswer',compact('question','title'));
    }

    public function posts()
    {
        $posts=Post::all();
        $title='لیست مقالات ';
        return view('posts',compact('posts','title'));
    }
    public function service($id=null)
    {
        $category=Category::findOrFail($id);
        $title=$category->name;
        return view('service',compact('category','title'));
    }

    public function contact()
    {
        $categories=Category::all();
        $title='تماس با ما';
        return view('contactus',compact('categories','title'));
    }


}
