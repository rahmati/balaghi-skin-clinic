<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Auth::check()){
            if((Auth::User()->isAdmin())){

                return $next($request);

            }else{

                return redirect('/')->with('message','برای دسترسی به این صفحه باید وارد شوید');
            }

        }
        return redirect('/')->with('message','برای دسترسی به این صفحه باید وارد شوید');

    }
}
