<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EnumRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category' => 'required|string',
            'name' =>'required|string',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'لطفا نام متغیر را وارد کنید',
            'name.string' => 'لطفا نام متغیر را به حروف  وارد کنید',
            'category.required' => 'لطفا گروه متغیر را وارد کنید',
            'category.string' => 'لطفا گروه متغیر را به حروف  وارد کنید',

        ];
    }
}
