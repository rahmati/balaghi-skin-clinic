<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules= [
            'title' => 'required|string|max:255|min:3',
            'body' =>'required|string|max:255|min:3',
            'user_id' =>'required',
            'category_id' =>'required',
            'slug' =>'required|string|max:255|min:3',

        ];
        if(Request::isMethod('POST')){
            $rules +=array(  'photo_id'=>'required|image');

        }
        return $rules;
    }

    public function messages()
    {
        return [
            'title.required' => 'لطفا نام پست را وارد کنید',
            'title.string' => 'لطفا نام پست را به حروف  وارد کنید',
            'title.min' => 'لطفا پست خود را وارد کنید',
            'title.max' => 'لطفا پست خود را وارد کنید',
            'body.required' => 'لطفا متن پست را وارد کنید',
            'body.string' => 'لطفا متن پست را به حروف  وارد کنید',
            'body.min' => 'لطفا متن پست را وارد کنید',
            'body.max' => 'لطفا متن پست  را وارد کنید',
            'photo_id.required'=>'لطفا عکس پست را وارد کنید',
            'user_id.required'=>'لطفا نویسنده را وارد کنید',
            'category_id.required'=>'لطفا گروه خدماتی پست را وارد کنید',
            'slug.required' => 'لطفا لینک  پست را وارد کنید',
            'slug.string' => 'لطفا لینک پست را به حروف  وارد کنید',
            'slug.min' => 'لطفا لینک پست خود را وارد کنید',
            'slug.max' => 'لطفا لینک پست خود را وارد کنید',
        ];
    }
}
