<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class QuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules= [
            'question' => 'required|string|min:10',
            'category_id' =>'required',
        ];

        if(Request::isMethod('PATCH')){
            $rules =array( 'answer'=>'required|string|min:10');

        }
        return $rules;
    }

    public function messages()
    {
        return [
            'question.required' => 'لطفا سوال خود را وارد کنید',
            'question.string' => 'لطفا سوال خود را وارد کنید',
            'question.min' => 'لطفا  سوال خود را وارد کنید',
            'answer.required' => 'لطفا پاسخ خود را وارد کنید',
            'answer.string' => 'لطفا پاسخ خود را وارد کنید',
            'answer.min' => 'لطفا  پاسخ خود را وارد کنید',
            'category_id.required' => 'لطفا گروه سوات خود را مشخص کنید',
        ];
    }
}
