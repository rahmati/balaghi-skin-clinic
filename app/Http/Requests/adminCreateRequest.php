<?php

namespace App\Http\Requests;

use App\Http\Middleware\TrimStrings;
use Illuminate\Foundation\Http\FormRequest;

class adminCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('user') ? ',' . $this->route('user') : '';

        return [
            'first_name' => 'required|string|max:255|min:3',
            'last_name' =>'required|string|max:255|min:3',
            'email' => 'required|email|unique:users,email' . $id,
            'password' => 'required|min:6',
            'enum_user_group_id' => 'required',

        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => 'لطفا نام خود را وارد کنید',
            'first_name.string' => 'لطفا نام خود را به حروف  وارد کنید',
            'first_name.min' => 'لطفا نام خود را وارد کنید',
            'first_name.max' => 'لطفا نام خود را وارد کنید',
            'last_name.required'  => 'لطفا نام خانوادگی خود را وارد کنید',
            'last_name.string' => 'لطفا نام خانوادگی خود را به حروف  وارد کنید',
            'last_name.min' => 'لطفا نام خانوادگی خود را وارد کنید',
            'last_name.max' => 'لطفا نام خانوادگی خود را وارد کنید',
            'email.required'  => 'لطفا پست الکترونیک خود را وارد کنید',
            'email.email'  => 'لطفا پست الکترونیک خود را وارد کنید',
            'email.unique'  => 'این پست الکترونیک تکراری است',
            'password.required'  => 'لطفا رمز عبور خود را وارد کنید',
            'password.min'  => 'لطفا برای رمز عبور حداقل شش حرف وارد کنید',
            'enum_user_group_id.required'  => 'لطفا گروه کاربری خود را وارد کنید',
        ];
    }
}
