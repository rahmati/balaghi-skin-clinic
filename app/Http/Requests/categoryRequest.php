<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class categoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

       $rules= [
            'name' => 'required|string|max:255|min:3',
            'body' =>'required|string|max:255|min:3',

        ];
       if(Request::isMethod('POST')){
           $rules +=array(  'photo_id'=>'required|image');

       }
       return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'لطفا نام خدمات را وارد کنید',
            'name.string' => 'لطفا نام خدمات را به حروف  وارد کنید',
            'name.min' => 'لطفا خدمات خود را وارد کنید',
            'name.max' => 'لطفا خدمات خود را وارد کنید',
            'body.required' => 'لطفا متن خدمات را وارد کنید',
            'body.string' => 'لطفا متن خدمات را به حروف  وارد کنید',
            'body.min' => 'لطفا متن خدمات را وارد کنید',
            'body.max' => 'لطفا متن خدمات  را وارد کنید',
            'photo_id.required'=>'لطفا عکس خدمات را وارد کنید'

        ];
    }
}
