<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable=['image'];

    protected $uploads='/images/';

    public function getimageattribute($photo){

        return $this->uploads.$photo;
    }
}
