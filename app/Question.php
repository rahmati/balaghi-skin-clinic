<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{

    protected $fillable=['question','answer','doctor_id','patient_id','category_id'];

    public function doctor(){
        return $this->belongsTo('App\User','doctor_id');
    }

    public function patient(){

        return $this->belongsTo('App\User','patient_id');
    }

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    public function category(){
        return $this->belongsTo('App\Category','category_id');
    }

}
