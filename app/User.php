<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'enum_user_group_id',
        'photo_id',
        'is_active',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];


    public function enum()
    {
       return $this->belongsTo('App\Enum', 'enum_user_group_id');
    }

    public function isAdmin()
    {
        if ($this->enum->name == 'Admin' && $this->is_active==1) {
            return true;
        }
        return false;
    }

    public function isMember(){

        if ($this->enum->name == 'Member') {
            return true;
        }
        return false;
    }

    public function photo(){

        return $this->belongsTo('App\Photo');
    }
    public function post(){
        return $this->hasMany('App\Post');
    }

    public function question(){

        return $this->hasMany('App\Question');
    }
}
