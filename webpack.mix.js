const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

mix.sass('resources/css/style.scss','public/css/style.css');

mix.styles(['resources/css/bootstrap-theme.css'], 'public/css/bootstrap.css');
mix.styles(['resources/css/bootstrap.min.css'], 'public/assets/css/bootstrap.min.css');
mix.styles(['resources/css/rtl.css'], 'public/css/rtl.css');
mix.styles(['resources/css/jquery-ui.min.css'], 'public/css/jquery.css');
mix.styles(['resources/css/AdminLTE.css'], 'public/css/AdminLTE.min.css');
mix.styles(['resources/css/skins/skin-purple.css'], 'public/css/skins/skin-purple.css');
mix.styles(['resources/css/hover-min.css'], 'public/css/hover.css');
mix.styles(['resources/bower_components/font-awesome/css/fontawesome.min.css'], 'public/css/skins/font-awesome.css');
mix.styles(['resources/bower_components/font-awesome/css/font-awesome-panel.min.css'], 'public/css/skins/font-awesome-panel.css');
mix.styles(['resources/bower_components/Ionicons/css/ionicons.css'], 'public/css/ionicons.css');
mix.styles(['resources/bower_components/slick/slick.css'], 'public/css/slick.css');


mix.scripts(['resources/bower_components/jquery/dist/jquery.js'], 'public/js/jquery/dist/jquery.js');
mix.scripts(['resources/bower_components/bootstrap/dist/js/bootstrap.min.js'], 'public/js/bootstrap/dist/js/bootstrap.min.js');
mix.scripts(['resources/js/bootstrap.min.js'],'public/assets/js/bootstrap.min.js');
mix.scripts(['resources/js/adminlte.js'], 'public/js/adminlte.min.js');
mix.scripts(['resources/js/sweetalert.js'], 'public/js/sweetalert.js');
mix.scripts(['resources/bower_components/slick/slick.js'], 'public/js/slick.js');
mix.scripts(['resources/bower_components/persian-date/persian-date.js'], 'public/js/persian-date.js');
mix.scripts(['resources/bower_components/persian-date/persian-date-0.1.8.min.js'], 'public/js/persian-date-0.18.js');


